package com.company;

import com.company.service.ProductService;
import com.company.service.impl.BuyerServiceImpl;
import com.company.service.impl.OrderServiceImpl;
import com.company.service.impl.ProductServiceImpl;

public class Main {

    public static void main(String[] args) {
	// write your code here
        ProductServiceImpl productService= new ProductServiceImpl();

        System.out.println("\n... Products.......\n");
        productService.viewProducts();

        System.out.println("\n... Products of a particular seller.......\n");
        productService.viewProductsForASeller(1);

        System.out.println("\n... placing an order.......\n");
        OrderServiceImpl orders= new OrderServiceImpl();
        orders.placeAnOrder(5,3);
        orders.placeAnOrder(1,2);
        orders.placeAnOrder(1,3);

        orders.placeAnOrder(2,3);
        orders.placeAnOrder(1,1);
        orders.placeAnOrder(4,1);

        System.out.println("\n... View Orders of a buyer......\n");
        BuyerServiceImpl buyerService= new BuyerServiceImpl();
        buyerService.viewOrderOfBuyer(3);

        System.out.println("\n... view orders particular seller.......\n");
        orders.viewOrderForSeller(1);

    }
}
