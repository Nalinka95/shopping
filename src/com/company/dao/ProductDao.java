package com.company.dao;

import com.company.bo.Buyer;
import com.company.bo.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductDao {
    private static ProductDao productDao;
    private List<Product> products;

    private ProductDao() {
        products = new ArrayList<Product>();
        initProducts();
    }

    public static ProductDao getInstance() {
        productDao = productDao == null ? new ProductDao() : productDao;
        return productDao;
    }
    private void initProducts(){
        Product product1= new Product(1,1,"OnePlus7",80000,12);
        addProduct(product1);
        Product product2= new Product(2,2,"OnePlus7T",170000,10);
        addProduct(product2);
        Product product3= new Product(3,1,"OnePlus6",90000,9);
        addProduct(product3);
        Product product4= new Product(4,1,"OnePlus7 Pro",281000,2);
        addProduct(product4);
        Product product5= new Product(5,3,"iPhone 11",220000,20);
        addProduct(product5);
        Product product6= new Product(6,1,"iPhone 11 pro",80000,1);
        addProduct(product6);
        Product product7= new Product(7,4,"iPhone 11 pro max",80000,12);
        addProduct(product7);
    }

    public List<Product> getProducts() {
        return products;
    }

    public void addProduct(Product newProduct) {
        products.add(newProduct);
    }

    public void removeProduct(int productId) {
        products.remove(findProductById(productId));
    }

    public void updateProduct(Product updateProduct) {
        for (int index = 0; index < products.size(); index++) {
            if(products.get(index).getProductId()==updateProduct.getProductId()){
                products.set(index,updateProduct);
            }
        }

    }
    public void updateProductQuantity(int productId,int changingQuantity){
        for (int index = 0; index < products.size(); index++) {
            if(products.get(index).getProductId()==productId){
                Product currentProduct =products.get(index);
                Product updateProductWithNewQuantity=new Product(productId,currentProduct.getSellerId(),currentProduct.getProductName(),currentProduct.getProductPrice(),changingQuantity);
                products.set(index,updateProductWithNewQuantity);
            }
        }
    }

    public Product findProductById(int productId) {
        for (Product currentProduct: products) {
            if (currentProduct.getProductId() == productId)
                return currentProduct;
        }
        return null;
    }

    private void writeToFile(Product storeProduct) {

    }

    private Product readFromFile() {
        Product readProduct = null;
        return readProduct;
    }
}
