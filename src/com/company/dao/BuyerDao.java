package com.company.dao;

import com.company.bo.Buyer;
import com.company.bo.Order;
import com.company.bo.Seller;

import java.util.ArrayList;
import java.util.List;

public class BuyerDao {
    private static BuyerDao buyerDao;
    private List<Buyer> buyers;

    private BuyerDao() {
        buyers = new ArrayList<Buyer>();
        initBuyers();
    }

    public static BuyerDao getInstance() {
        buyerDao = buyerDao == null ? new BuyerDao() : buyerDao;
        return buyerDao;
    }

    public List<Buyer> getBuyers() {
        return buyers;
    }

    public Buyer getDetailsById(int buyerId) {
        return findBuyerById(buyerId);
    }

    public void addSBuyer(Buyer newBuyer) {
        buyers.add(newBuyer);
    }

    public void removeBuyer(int buyerId) {
        buyers.remove(findBuyerById(buyerId));
    }

    public void updateBuyer(Buyer updateBuyer) {
        for (int index = 0; index < buyers.size(); index++) {
            if(buyers.get(index).getBuyerId()==updateBuyer.getBuyerId()){
                buyers.set(index,updateBuyer);
            }
        }

    }

    public Buyer findBuyerById(int buyerId) {
        for (Buyer currentBuyer: buyers) {
            if (currentBuyer.getBuyerId() == buyerId)
                return currentBuyer;
        }
        return null;
    }
    private void initBuyers(){
        Buyer buyer1= new Buyer(1,"Nalinka");
        addSBuyer(buyer1);
        Buyer buyer2= new Buyer(3,"salitha");
        addSBuyer(buyer2);
        Buyer buyer3= new Buyer(4,"Charith");
        addSBuyer(buyer3);
    }

    private void writeToFile(Buyer storeBuyer) {

    }

    private Buyer readFromFile() {
        Buyer readBuyer = null;
        return readBuyer;
    }
}
