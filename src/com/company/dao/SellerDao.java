package com.company.dao;

import com.company.bo.Seller;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

public class SellerDao {
    private static SellerDao sellerDao;
    private List<Seller> sellers;

    private SellerDao() {
        sellers = new ArrayList<Seller>();
        initSeller();
    }

    public static SellerDao getInstance() {
        sellerDao = sellerDao == null ? new SellerDao() : sellerDao;
        return sellerDao;
    }

    public List<Seller> getSellers() {
        return sellers;
    }

    public void addSeller(Seller newSeller){
        sellers.add(newSeller);
    }
    public void removeSeller(int sellerId){
        sellers.remove(findSellerById(sellerId));
    }
    public void updateSeller(Seller updateSeller){
        for (int index = 0; index < sellers.size(); index++) {
            if(sellers.get(index).getSellerId()==updateSeller.getSellerId()){
                sellers.set(index,updateSeller);
            }
        }
    }
    public Seller findSellerById(int sellerId){
        for (Seller currentSeller : sellers) {
            if (currentSeller.getSellerId()== sellerId)
                return currentSeller;
        }
        return new Seller(0,"None");
    }
    private void initSeller(){
        Seller seller1= new Seller(1,"Yasas");
        addSeller(seller1);
        Seller seller2= new Seller(2,"Isuru");
        addSeller(seller2);
        Seller seller3= new Seller(3,"Heshan");
        addSeller(seller3);
        Seller seller4= new Seller(4,"Gota");
        addSeller(seller4);
    }
    private void writeToFile(Seller storeSeller){

    }
    private Seller readFromFile(){
        Seller readSeller=null;
        return readSeller;
    }
}
