package com.company.dao;

import com.company.bo.Order;
import com.company.bo.Product;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class OrderDao {
    private static OrderDao orderDao;
    private List<Order> orders;

    private OrderDao() {
        orders = new ArrayList<Order>();
    }

    public static OrderDao getInstance() {
        orderDao = orderDao == null ? new OrderDao() : orderDao;
        return orderDao;
    }

    public Order getOrderDetailsById(int orderId) {
        return findOrderById(orderId);

    }

    public List<Order> getOrderForBuyer(int buyerId) {
        List<Order> buyersOrder = new ArrayList<Order>();
        for (Order currentOrder : orders) {
            if (currentOrder.getBuyerId() == buyerId)
                buyersOrder.add(currentOrder);
        }
        return buyersOrder;

    }

    public List<Order> getOrders() {
        return orders;
    }

    public void addOrder(Order newOrder) {
        orders.add(newOrder);
    }

    public void removeOrder(int orderId) {
        orders.remove(findOrderById(orderId));
    }

    public void updateOrder(Order updateOrder) {
        for (int index = 0; index < orders.size(); index++) {
            if (orders.get(index).getOrderId() == updateOrder.getOrderId()) {
                orders.set(index, updateOrder);
            }
        }
    }

    public Order findOrderById(int orderId) {
        for (Order currentOrder : orders) {
            if (currentOrder.getOrderId() == orderId)
                return currentOrder;
        }
        return null;
    }

    private void writeToFile(Order storeOrder) {

    }

    private Order readFromFile() {
        Order readOrder = null;
        return readOrder;
    }
}
