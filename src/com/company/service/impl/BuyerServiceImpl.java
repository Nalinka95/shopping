package com.company.service.impl;

import com.company.bo.Order;
import com.company.bo.Product;
import com.company.bo.Seller;
import com.company.dao.BuyerDao;
import com.company.dao.OrderDao;
import com.company.dao.ProductDao;
import com.company.dao.SellerDao;

import java.util.List;

public class BuyerServiceImpl {
    public void viewOrderOfBuyer(int buyerId){
        List<Order> listOfBuyersOder=OrderDao.getInstance().getOrderForBuyer(buyerId);
        List<Product> listOfProduct=ProductDao.getInstance().getProducts();
        List<Seller> listOfSeller=SellerDao.getInstance().getSellers();

        String sellerName="";
        int sellerId=0;
        String buyerName;
        String productName="";
        double productPrice=0.0;
        int orderedQuantity;
        int orderId;

        for(Order currentOrder: listOfBuyersOder){
            int productId=currentOrder.getProductId();
            for(Product product :listOfProduct){
                if(product.getProductId()==productId){
                    productPrice=product.getProductPrice();
                    productName=product.getProductName();
                    sellerId=product.getSellerId();
                }
            }
            for(Seller seller: listOfSeller){
                if(seller.getSellerId()==sellerId){
                    sellerName=seller.getSellerName();
                }
            }
            buyerName= BuyerDao.getInstance().findBuyerById(buyerId).getBuyerName();
            orderedQuantity= currentOrder.getOrderQuentity();
            orderId=currentOrder.getOrderId();
            System.out.println("Seller Name : "+sellerName+" Buyer Name : "+buyerName+" Product Name : "+productName+" Ordered Quantity  : "+orderedQuantity+" Order Id: "+orderId+" Price : "+productPrice);

        }
    }
}
