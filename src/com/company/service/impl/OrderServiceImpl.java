package com.company.service.impl;

import com.company.bo.Buyer;
import com.company.bo.Order;
import com.company.bo.Product;
import com.company.dao.BuyerDao;
import com.company.dao.OrderDao;
import com.company.dao.ProductDao;
import com.company.dao.SellerDao;
import com.sun.org.apache.xpath.internal.operations.Or;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class OrderServiceImpl {
    private static int orderIds = 0;

    public void placeAnOrder(int productId, int buyerId) {
        int availableQuantity = ProductDao.getInstance().findProductById(productId).getAvailableQuantity();
        int orderQuantity = 1;
        if (availableQuantity >= orderQuantity) {
            orderIds++;
            int newOrderId = orderIds;
            DateTimeFormatter date = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            LocalDateTime now = LocalDateTime.now();
            String orderedDate=date.format(now);
            Order newOrder = new Order(newOrderId, buyerId, productId, orderQuantity, orderedDate);
            availableQuantity--;
            OrderDao.getInstance().addOrder(newOrder);
            ProductDao.getInstance().updateProductQuantity(productId, availableQuantity);
            if (availableQuantity == 0) {
                ProductDao.getInstance().removeProduct(productId);
            }
        }

    }
    public List<Order> getOrderForSeller(int sellerId) {
        List<Order> sellersOrder = new ArrayList<Order>();
        for (Order currentOrder : OrderDao.getInstance().getOrders()) {
            for (int index = 0; index < ProductDao.getInstance().getProducts().size(); index++) {
                Product currentProduct = ProductDao.getInstance().getProducts().get(index);
                if (currentOrder.getProductId() == currentProduct.getProductId() && currentProduct.getSellerId() == sellerId) {
                    sellersOrder.add(currentOrder);
                }
            }
        }
        return sellersOrder;

    }
    public Order getOrderForBuyer(int buyerId){
        return (Order)OrderDao.getInstance().getOrderForBuyer(buyerId);
    }
    public void viewOrderForSeller(int sellerId){
        List<Order> ordersOfSeller= getOrderForSeller(sellerId);
        List<Buyer> listOfBuyers=BuyerDao.getInstance().getBuyers();

        String sellerName="";
        String buyerName="";
        String productName="";
        double productPrice=0.0;
        int orderedQuantity;
        int orderId;

        for(Order currentOrder: ordersOfSeller){
            sellerName= SellerDao.getInstance().findSellerById(sellerId).getSellerName();
            int buyerId=currentOrder.getBuyerId();
            for(Buyer buyer:listOfBuyers){
                if(buyer.getBuyerId()==buyerId){
                    buyerName=buyer.getBuyerName();
                    break;
                }
            }
            productName=ProductDao.getInstance().findProductById(currentOrder.getProductId()).getProductName();
            productPrice= ProductDao.getInstance().findProductById(currentOrder.getProductId()).getProductPrice();
            orderedQuantity= currentOrder.getOrderQuentity();
            orderId=currentOrder.getOrderId();
            System.out.println("Seller Name : "+sellerName+" Buyer Name : "+buyerName+" Product Name : "+productName+" Ordered Quantity  : "+orderedQuantity+" Order Id: "+orderId+" Price : "+productPrice);
        }
    }
}
