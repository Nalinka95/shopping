package com.company.service.impl;

import com.company.bo.Product;
import com.company.bo.Seller;
import com.company.dao.ProductDao;
import com.company.dao.SellerDao;
import com.company.service.ProductService;

public class ProductServiceImpl implements ProductService {
    @Override
    public void viewProducts() {
        for(Product product: ProductDao.getInstance().getProducts()){
            Seller currentSeller=SellerDao.getInstance().findSellerById(product.getSellerId());
            System.out.println("Product name : "+product.getProductName()+" Available Quantity : "+product.getAvailableQuantity()+" Seller Name : "+currentSeller.getSellerName()+" Seller id : "+currentSeller.getSellerId()+" Product Id : "+product.getProductId());
        }

    }
    public void viewProductsForASeller(int sellerId){
        for(Product product: ProductDao.getInstance().getProducts()){
            if(product.getSellerId()==sellerId)
                System.out.println("Product name : "+product.getProductName()+" Available Quantity : "+product.getAvailableQuantity()+" Seller Name : "+ SellerDao.getInstance().findSellerById(product.getSellerId()).getSellerName()+" Seller Id : "+sellerId+" Product Id : "+product.getProductId());
        }
    }
}
