package com.company.bo;

import java.util.Objects;

public class Buyer {
    private int buyerId;
    private String buyerName;

    public Buyer(int buyerId, String buyerName) {
        setBuyerId(buyerId);
        setBuyerName(buyerName);
    }

    public int getBuyerId() {
        return buyerId;
    }

    private void setBuyerId(int buyerId) {
        this.buyerId = buyerId;
    }

    public String getBuyerName() {
        return buyerName;
    }

    private void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Buyer)) return false;
        Buyer buyer = (Buyer) o;
        return getBuyerId() == buyer.getBuyerId() &&
                Objects.equals(getBuyerName(), buyer.getBuyerName());
    }

}
