package com.company.bo;

import java.util.Objects;

public class Order {
    private int orderId;
    private int buyerId;
    private int productId;
    private int orderQuentity;
    private String date;

    public Order(int orderId, int buyerId, int productId, int orderQuentity, String date) {
        setOrderId(orderId);
        setBuyerId(buyerId);
        setProductId(productId);
        setOrderQuentity(orderQuentity);
        setDate(date);
    }

    public int getOrderId() {
        return orderId;
    }

    private void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getBuyerId() {
        return buyerId;
    }

    private void setBuyerId(int buyerId) {
        this.buyerId = buyerId;
    }

    public int getProductId() {
        return productId;
    }

    private void setProductId(int productId) {
        this.productId = productId;
    }

    public int getOrderQuentity() {
        return orderQuentity;
    }

    private void setOrderQuentity(int orderQuentity) {
        this.orderQuentity = orderQuentity;
    }

    public String getDate() {
        return date;
    }

    private void setDate(String date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return getOrderId() == order.getOrderId() &&
                getBuyerId() == order.getBuyerId() &&
                getProductId() == order.getProductId() &&
                getOrderQuentity() == order.getOrderQuentity() &&
                Objects.equals(getDate(), order.getDate());
    }

}
