package com.company.bo;

import java.util.Objects;

public class Seller {
    private int sellerId;
    private String sellerName;

    public Seller(int sellerId, String sellerName) {
        setSellerId(sellerId);
        setSellerName(sellerName);
    }

    public int getSellerId() {
        return sellerId;
    }

    private void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    private void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Seller)) return false;
        Seller seller = (Seller) o;
        return getSellerId() == seller.getSellerId() &&
                Objects.equals(getSellerName(), seller.getSellerName());
    }

}
