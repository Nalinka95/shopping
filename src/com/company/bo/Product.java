package com.company.bo;

import java.util.Objects;

public class Product {
    private int productId;
    private int sellerId;
    private String productName;
    private double productPrice;
    private int availableQuantity;

    public Product(int productId, int sellerId, String productName, double productPrice, int availableQuantity) {
        setProductId(productId);
        setSellerId(sellerId);
        setProductName(productName);
        setProductPrice(productPrice);
        setAvailableQuantity(availableQuantity);
    }

    public int getProductId() {
        return productId;
    }

    private void setProductId(int productId) {
        this.productId = productId;
    }

    public int getSellerId() {
        return sellerId;
    }

    private void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getProductName() {
        return productName;
    }

    private void setProductName(String productName) {
        this.productName = productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    private void setProductPrice(double productPrice) {
        this.productPrice = productPrice;
    }

    public int getAvailableQuantity() {
        return availableQuantity;
    }

    private void setAvailableQuantity(int availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return getProductId() == product.getProductId() &&
                getSellerId() == product.getSellerId() &&
                Double.compare(product.getProductPrice(), getProductPrice()) == 0 &&
                getAvailableQuantity() == product.getAvailableQuantity() &&
                Objects.equals(getProductName(), product.getProductName());
    }

}
